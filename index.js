// export const printNice = () => {
//   console.log("nicee");
// };

export class Person {
  greatName = "wow";

  constructor(name) {
    this.name = name;
    this.changeGreatName();
  }

  changeGreatName() {
    this.greatName = "it should be a great name";
  }

  introduce() {
    console.log(`Hello, my name is ${this.greatName}`);
  }
}

export class Wuhan extends Person {
  constructor() {
    super();
  }

  wuhanName() {
    console.log(`Wuhaan inside ${this.greatName}`);
  }
}
